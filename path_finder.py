from astar import AStar
import math


class PathFinder(AStar):
    ROT_SPEED = math.pi / 8     # rad/s
    STOP_TIME = 0.5     # sec
    SPEED = 0.2   # m/s
    NODE_SIZE = 0.1     # m

    def heuristic_cost_estimate(self, current, goal):  # Return raw distance between 2 nodes (hypot)
        (x1, y1) = current.position
        (x2, y2) = goal.position
        return(math.hypot(x2 - x1, y2 - y1))

    def neighbors(self, node):  # Return node neighbors
        return node.neighbors

    def distance_between(self, prev, current, next):  # Return the time to go from one node to one of its neighbors
        angle = current.angle(prev, next)
        rotTime = abs(math.pi - angle) / self.ROT_SPEED  # Time to turn

        if(rotTime > 0):
            rotTime += self.STOP_TIME

        mvTime = self.NODE_SIZE * self.SPEED

        if current.position[0] != next.position[0]:
            if current.position[1] != next.position[1]:
                mvTime *= math.sqrt(2)

        #time = rotTime + mvTime

        return mvTime


class Node:

    """ Define a node, a position on the map """

    def __init__(self, position, attrDesc=0):
        self.position = position    # (x, y)

        # Define obstacles and doors (permanent)
        if attrDesc == 0:
            self.permanentObstacle = False
            self.isDoor = False
            self.base = False
        elif attrDesc == 1:
            self.permanentObstacle = True
            self.isDoor = False
            self.base = False
        elif attrDesc == 2:
            self.permanentObstacle = False
            self.isDoor = True
            self.base = False
        elif attrDesc == 3:
            self.permanentObstacle = True
            self.isDoor = False
            self.base = True
        else:
            raise Exception(f"Unknown attribute descriptor: {attrDesc}")

        self.tempObstacle = False
        self.isDirty = True
        self.neighbors = []
        self.accessTo = ""

    def angle(self, prev, next):
        """Calculate angle between previous, current and next node"""
        if prev is None:
            return math.pi

        (xp, yp) = prev.position
        (xc, yc) = self.position
        (xn, yn) = next.position
        angle = math.atan2(yn - yc, xn - xc) - math.atan2(yp - yc, xp - xc)
        angle = angle % (2 * math.pi)
        return angle

    def isReachable(self):
        """ Test if the node is not an obstacle """
        if self.permanentObstacle or self.tempObstacle:
            return False
        else:
            return True

    def __repr__(self):
        """ Pretty printing """
        return(f'Node {self.position}')
