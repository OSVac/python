import json
import numpy as np

cellWidth = 10  # in cm

###########################################################################
# Salon 5x4m
livinWidth = int(5 * (100 / cellWidth))  # in m
livinHight = int(4 * (100 / cellWidth))  # in m

livin = np.zeros((livinWidth, livinHight), dtype=int)
livin[livinWidth-3:livinWidth, 3:11] = 1  # canap 60*160
livin[20-2:20, livinHight-3:livinHight] = 1  # meuble bureau 40*60
livin[0:6, 0:2] = 1  # bordel dans le coin  120*40

livin[livinWidth-1:livinWidth, 15:19] = 2  # porte salon-entrée 20*80
livin[1:5, livinHight-1:livinHight] = 2  # porte salon-cuisine 80*20

data = {'data_field': livin.tolist(), 'dim_cell_cm': cellWidth}

with open('map.json', 'w') as f:
    json.dump(data, f)
