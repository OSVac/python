from path_finder import Node, PathFinder
from robot import Robot
from math import pi
from server import OSVhttp
import asyncio
import numpy as np
import json

from multiprocessing import Process
import time
import os
import sys
import signal


def reduce_path(path):
    simplePath = []
    pNode = path[0]
    diff = (0, 0)
    for node in path:
        newDiff = tuple(map(lambda x, y: x - y, node.position, pNode.position))
        if diff != newDiff:
            diff = newDiff
            simplePath.append(pNode)
        pNode = node
    simplePath.append(path[-1])
    return simplePath

def load_map(file):
    with open(file, 'r') as f:
       data = json.load(f)

    map = []   # Construct map of nodes
    for y, line in enumerate(data['data_field']):
        tmpLine = []
        for x, cell in enumerate(line):
            node = Node((x, y), attrDesc=cell)
            tmpLine.append(node)
        map.append(tmpLine)

    """Assign neighbors"""
    for y, line in enumerate(map):
        for x, node in enumerate(line):
            neighbors = []
            for i in [-1, 0, 1]:
                for j in [-1, 0, 1]:
                    if not (j == 0 and i == 0):
                        if y+i >= 0 and x+j >= 0:
                            try:
                                if map[y+i][x+j].isReachable():
                                    neighbors.append(map[y+i][x+j])
                            except IndexError:
                                pass
            node.neighbors = neighbors
    return(map)


def httpserver(parent):
    http = OSVhttp(parent)
    loop = asyncio.get_event_loop()
    http.schedule(loop)

    try:
        loop.run_forever()
    finally:
        loop.stop()
        loop.close()


def on_signal(signum, frame):
    print("Time to reload")


def fork(target):
    signal.signal(signal.SIGHUP, on_signal)
    child = Process(target=target, args=(os.getpid(), ))
    child.start()
    return child


if __name__ == "__main__":
    cellWidth = 10  # in cm

    ###########################################################################
    # Salon 5x4m
    livinWidth = int(5 * (100 / cellWidth))  # in m
    livinHight = int(4 * (100 / cellWidth))  # in m

    livin = np.zeros((livinWidth, livinHight), dtype=int)
    livin[livinWidth-3:livinWidth, 3:11] = 1  # canap 60*160
    livin[20-2:20, livinHight-3:livinHight] = 1  # meuble bureau 40*60
    livin[0:6, 0:2] = 1  # bordel dans le coin  120*40

    livin[livinWidth-1:livinWidth, 15:19] = 2  # porte salon-entrée 20*80
    livin[1:5, livinHight-1:livinHight] = 2  # porte salon-cuisine 80*20

    flatMap = load_map('map.json')

    base = flatMap[0][-18]
    dest = flatMap[-1][-12]
    path = list(PathFinder().astar(base, dest))
    smol_path = reduce_path(path)
    angle = 0 * pi
    robot = Robot(base, angle)

    path = list(PathFinder().astar(base, dest))
    smol_path = reduce_path(path)
    print(smol_path)
    pathIter = iter(smol_path[1:])
    robot.goto(smol_path[1])
    try:
        child = fork(httpserver)
        while True:
            msg = robot.recv()
            if msg.code == 'N':     # Arrived at dest
                try:
                    robot.goto(next(pathIter))
                except StopIteration:
                    robot.stop()
                    break
            elif msg.code == 'B':
                for i, node in enumerate(path):
                    if (msg.param1, msg.param2) == node.position:
                        idx = i
                        break
                try:
                    path[idx+1].tempObstacle = True
                except IndexError:
                    pass

                path = list(PathFinder().astar(base, dest))
                smol_path = reduce_path(path)
                pathIter = iter(smol_path[2:])
                robot.goto(smol_path[1])
    finally:
        child.terminate()
        robot.reset()
