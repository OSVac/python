import serial
import math
from path_finder import Node


class Message:
    """ Define messages to talk to the esp """
    def __init__(self, code: str, param1: int, param2: int):
        self.code = code
        self.param1 = param1
        self.param2 = param2

    @classmethod
    def fromStr(cls, msg: str):  # Parse message string ("OPCODE, posX, posYg")
        params = msg.split(',')
        if len(params) != 3:
            return
        return cls(params[0], round(float(params[1])), round(float(params[2])))

    def __str__(self):
        """ Convert to string """
        return f"{self.code},{self.param1},{self.param2}"


class Robot:
    """ Communicate with the esp """
    def __init__(self, nodeBase: Node, angle):
        self._ser = serial.Serial('/dev/ttyUSB0', 115200)  # Init serial
        self.send(Message('I', nodeBase.position[0], nodeBase.position[1]))  # Send init message (init pos)
        angleDec = int(angle)
        angleFloat = int((angle % 1) * math.pow(10, 8))
        self.send(Message('A', angleDec, angleFloat))  # Init angle

    def recv(self, timeout=None) -> Message:
        """ Handle message reception, return a message class filled with informations """
        self._ser.timeout = timeout
        resp = self._ser.readline().decode("utf-8").strip('\r\n')  # Read \n terminated string from serial
        print(f"Debug: {resp}", flush=True)
        return Message.fromStr(resp)

    def send(self, data, retry=10):
        """ Handle message sending """
        for _ in range(retry):
            self._ser.write(f"{data}\r\n".encode("utf-8"))  # Send string message
            ack = self.recv(timeout=2)  # Wait for confirmation
            if ack.code == 'K':
                return ack
        else:
            raise Exception("Timeout reached! No response from esp.")

    def goto(self, nodeDest: Node):
        """ Wrapper to send goto message to esp """
        self.send(Message('D', nodeDest.position[0], nodeDest.position[1]))

    def stop(self):
        """ Wrapper to send stop message to esp """
        self.send(Message('S', 0, 0))

    def reset(self):
        """ Wrapper to send reset message to esp """
        self.send(Message('R', 0, 0))
