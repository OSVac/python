from path_finder import Node, PathFinder
from robot import Robot
from math import pi
from server import OSVhttp
import asyncio
import json

from multiprocessing import Process
import time
import os
import sys
import signal

class Control:
    def __init__(self, basePos: tuple, angle: int, file: str):
        self.flatMap = self.load_map(file)
        self.base = self.flatMap[basePos[0]][basePos[1]]
        self.angle = angle
        self.paused = False
        return

    def reduce_path(self, path):
        simplePath = []
        pNode = path[0]
        diff = (0, 0)
        for node in path:
            newDiff = tuple(map(lambda x, y: x - y, node.position, pNode.position))
            if diff != newDiff:
                diff = newDiff
                simplePath.append(pNode)
            pNode = node
        simplePath.append(path[-1])
        return simplePath

    def load_map(self, file):
        with open(file, 'r') as f:
            data = json.load(f)

        map = []   # Construct map of nodes
        for y, line in enumerate(data['data_field']):
            tmpLine = []
            for x, cell in enumerate(line):
                node = Node((x, y), attrDesc=cell)
                tmpLine.append(node)
            map.append(tmpLine)

        """Assign neighbors"""
        for y, line in enumerate(map):
            for x, node in enumerate(line):
                neighbors = []
                for i in [-1, 0, 1]:
                    for j in [-1, 0, 1]:
                        if not (j == 0 and i == 0):
                            if y+i >= 0 and x+j >= 0:
                                try:
                                    if map[y+i][x+j].isReachable():
                                        neighbors.append(map[y+i][x+j])
                                except IndexError:
                                    pass
                node.neighbors = neighbors
        return(map)


    def httpserver(self, parent):
        http = OSVhttp(parent)
        loop = asyncio.get_event_loop()
        http.schedule(loop)

        try:
            loop.run_forever()
        finally:
            loop.stop()
            loop.close()


    def on_sighup(self, signum, frame):
        self.flatMap = self.load_map('map.json')

    def on_sigpwr(self, signum, frame):
        self.robot.reset()
        os.system('sudo reboot')

    def on_sigstop(self, signum, frame):
        self.paused = True

    def on_sigcont(self, signum, frame):
        self.paused = False

    def fork(self, target):
        signal.signal(signal.SIGHUP, self.on_sighup)
        signal.signal(signal.SIGPWR, self.on_sigpwr)
        signal.signal(signal.SIGCONT, self.on_sigcont)
        signal.signal(signal.SIGSTOP, self.on_sigstop)
        child = Process(target=target, args=(os.getpid(), ))
        child.start()
        return child

    def run(self, destPos: tuple):
        self.dest = self.flatMap[destPos[0]][destPos[1]]
        path = list(PathFinder().astar(self.base, self.dest))
        smol_path = self.reduce_path(path)
        self.robot = Robot(self.base, self.angle)

        pathIter = iter(smol_path[1:])
        try:
            child = self.fork(self.httpserver)
            while True:                
                msg = self.robot.recv()
                if msg.code == 'N':     # Arrived at dest
                    try:
                        self.robot.goto(next(pathIter))
                    except StopIteration:
                        self.robot.stop()
                        break
                elif msg.code == 'B':
                    for i, node in enumerate(path):
                        if (msg.param1, msg.param2) == node.position:
                            idx = i
                            break
                    try:
                        path[idx+1].tempObstacle = True
                    except IndexError:
                        pass

                    path = list(PathFinder().astar(self.base, self.dest))
                    smol_path = self.reduce_path(path)
                    pathIter = iter(smol_path[2:])
                    self.robot.goto(smol_path[1])
        finally:
            child.terminate()
            self.robot.reset()