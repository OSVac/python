import asyncio
from server import OSVhttp

http = OSVhttp()

loop = asyncio.get_event_loop()
http.schedule(loop)

try:
    loop.run_forever()
finally:
    loop.stop()
    loop.close()