
var s_row = 0;
var s_col = 0;
var l_col = 0;
var l_row = 0;
var state_selected ="+";
var state_empty="";
var state_wall="1";
var tol_line =0;
var dim_cell_cm = 10 //une cellule represente 10cm carre
var nb_cell_row;//nb de cellules par ligne
var nb_cell_col; //nb de cellules par col
var door_width = 0.8; //m
var selector_is_set = false;
var points_piece = new Array();
var current_cell ;
var canvas_ctx;
var canvas;
var lastMouse = {x:0,y:0};
function init()
{
  var board = document.getElementById("board");
  canvas =  document.getElementById("canvas_container");
  if (canvas!=null){
	//  canvas_offset = canvas.offset();
	  if (canvas.getContext)
		  canvas_ctx = canvas.getContext('2d');
  }
  if (board == null)
  {
	  alert("Error: board not found");
  }
  var width_m = 15; // en m
  var height_m =30; // en m
  nb_cell_row = m_to_nb_cell(height_m);
  nb_cell_col = m_to_nb_cell(width_m);
  //alert("Tableau de "+nb_cell_row+" sur "+nb_cell_col);
  window.oncontextmenu = function ()
  {
    return false;     // cancel contextual menu
  }
  tableCreate(board,nb_cell_row,nb_cell_col);
  //init canvas
  canvas.setAttribute("width",board.offsetWidth);
  canvas.setAttribute("height",board.offsetHeight);  
  canvas_ctx.lineWidth = 2;
  canvas_ctx.strokeStyle = '#ff0000';
}

function m_to_nb_cell(length)
{
   return Math.floor(length*100 / dim_cell_cm)
}
function nb_cell_to_m(nb)
{
  return nb*dim_cell_cm/100;
}
function distance_between(c1,c2)
{
	var w = c1.cellIndex - c2.cellIndex;
	var h = c1.parentElement.rowIndex - c2.parentElement.rowIndex;
	var d = Math.sqrt( Math.pow( w, 2) + Math.pow( h,2 ) );
	return nb_cell_to_m(d);
}
function tableCreate(tbl,nb_cell_row,nb_cell_col){
  for(var i = 0; i < nb_cell_row; i++){
      var tr = tbl.insertRow();
      for(var j = 0; j < nb_cell_col; j++){
              var td = tr.insertCell();
              td.appendChild(document.createTextNode(''));
              td.setAttribute("class","cell");
			  //listener
			  td.addEventListener("contextmenu",click_cell);
              td.addEventListener("click",click_cell);
			  td.addEventListener("mouseover",show_distance);
      }
  }
  current_cell = tbl.rows[0].cells[0];
  document.body.appendChild(tbl);
}
function select_state(el,state)
{
  if (state)
  {
    el.innerHTML=state_selected;
    current_cell = el;
    l_row = s_row;
    l_col = s_col;
    s_row = el.parentElement.rowIndex;
    s_col = el.cellIndex;
  }
  else
  {
    el.innerHTML=state_empty;
  }
}
function unselect()
{
  select_state(current_cell,false);
  selector_is_set = false;
  // delete drawing help line
  canvas_ctx.clearRect(0,0,canvas.width,canvas.height);
  canvas_ctx.closePath();  
}
function select_cell(cell) //select cell and unselect others
{
	if (cell != null && cell !=undefined && current_cell != null && current_cell !=undefined){
		var former_cell = document.getElementById("board").rows[l_row].cells[l_col];
		select_state(current_cell,false);
		current_cell = cell;
		select_state(cell,true);	
		select_state(former_cell,false);
	}
}
function getMousePos(canvas, e) {
    return {
      x: e.clientX - canvas.offsetLeft,
      y: e.clientY - canvas.offsetTop
    };
}
function show_distance(e){
    if (!e) var e = window.event;                // Get the window event
    e.cancelBubble = true;                       // IE Stop propagation
    if (e.stopPropagation) e.stopPropagation();  // Other Broswers
	//print distance between hovered cell and current_cell
	if (selector_is_set){
		canvas_ctx.clearRect(0,0,canvas.width,canvas.height);
		canvas_ctx.beginPath();
		canvas_ctx.moveTo(lastMouse.x,lastMouse.y);
		canvas_ctx.lineTo(getMousePos(canvas,e).x, getMousePos(canvas,e).y);
		canvas_ctx.stroke();
	}
	// print distance
	if (e.target != null && current_cell != null)
		document.getElementById("distance").value = distance_between(e.target , current_cell).toFixed(2) + " m"; 
	
}
function click_cell(e){
    if (!e) var e = window.event;                // Get the window event
    e.cancelBubble = true;                       // IE Stop propagation
    if (e.stopPropagation) e.stopPropagation();  // Other Broswers
	var right_is_pressed;
    if (e.which == 3 || e.button == 2) right_is_pressed = true;  //right click
    if (e.which == 1 || e.button == 0) right_is_pressed = false; //left click;
	var tool = checkedTool();
	lastMouse.x = getMousePos(canvas,e).x;
	lastMouse.y = getMousePos(canvas,e).y;
    if (tool=="line" || tool== "erase_line")
    {
      if (!selector_is_set )
      {
        selector_is_set = true;
        select_cell(e.target);
        points_piece.push([s_row,s_col]);
      }
      else
      {
        select_cell(e.target);
        if (tool=="line")
        {
		  if (right_is_pressed)
			make_line(prompt("Longueur du pan de mur (m)"),"wall");
		  else 
			make_line(0,"wall");			
          if (points_piece[0][0]==s_row && points_piece[0][1]== s_col && points_piece.length>1)
          {
            create_room();
          }
        } else if (tool=="erase_line")
        {
		  if (right_is_pressed)
			make_line(prompt("Longueur à effacer (m)"),"cell");
		  else
			make_line(0,"cell");			  
          unselect();
        }

      }
    }
    else if (tool=="door")
    {
      if (!selector_is_set)
      {
        selector_is_set = true;
        select_cell(this);
      }
      else
      {
        select_cell(this);
        make_door(door_width);
      }  
    }
    else if (tool=="charging")
    {
        selector_is_set = true;
        select_cell(this);
        current_cell.className=tool;
    }  
    
	
}
function checkedTool()
{
  var tool_box = document. getElementById("tool_box");
  for (var i=0; i < tool_box.length; i++)
  {
    if (tool_box[i].checked)
    {
      return tool_box[i].value;
    }
  }
  return "undefined";
}
function finish_room()
{
  var cell = document.getElementById("board").rows[points_piece[0][0]].cells[points_piece[0][1]];
  select_cell(cell);
  if (s_row-l_row==0 || s_col-l_col==0)
  {
    var distance = Math.sqrt(Math.pow(nb_cell_to_m(s_row-l_row),2)+Math.pow(nb_cell_to_m(s_col-l_col),2));
    make_line(distance);
    create_room();
  }
  else {
    alert("Please align walls before finishing");
  }
}

function create_room()
{
  points_piece = new Array();
  unselect();
}

function make_line(distance,type){
    var former_cell = document.getElementById("board").rows[l_row].cells[l_col];
    var col_diff = s_col-l_col;
    var row_diff = s_row-l_row;
    var start_row,start_col,end_row,end_col;
    current_cell = document.getElementById("board").rows[l_row].cells[l_col];
    // selected cells will correspond to either the real cell clicked or the specified length
	var nb_cell = distance ? m_to_nb_cell(distance) : Math.abs(col_diff) + Math.abs(row_diff); 
    if ( !(col_diff==0 && row_diff==0))
    {
        if (row_diff>0)
        {
          start_row = l_row;
          end_row = s_row;
        }
        else
        {
          start_row = s_row;
          end_row = l_row;
        }
        if (col_diff>0)
        {
          start_col = l_col;
          end_col = s_col;
        }
        else
        {
          start_col = s_col;
          end_col = l_col;
        }
        if (!(make_vertical(type,nb_cell)))
        {
          if (!make_horizontal(type,nb_cell))
          { //diagonal
             var row = col=0;
             for (var i=0;i<nb_cell;i++)
             {
                current_cell = document.getElementById("board").rows[l_row+row].cells[l_col+col];
				if (current_cell == null)
					return ;
                current_cell.className=type;
                row+=Math.sign(row_diff);
                current_cell = document.getElementById("board").rows[l_row+row].cells[l_col+col];
				if (current_cell == null)
					return;
                current_cell.className=type;
                col+=Math.sign(col_diff);
             }
          }
        }
        select_cell(current_cell);
        points_piece.push([s_row,s_col]);
    }


}

function make_door()
{
  if (!make_vertical("door",m_to_nb_cell(door_width)))
    make_horizontal("door",m_to_nb_cell(door_width));
  unselect();
}

function make_vertical(type,nb_cell)
{
  var i=0;
  if (Math.abs(s_col-l_col)<=tol_line)//vertical
  {
      var sens = Math.sign(s_row-l_row);
      if (l_row+nb_cell*sens<0)
        nb_cell = l_row+1;
      else if (l_row+nb_cell*sens>nb_cell_row)
        nb_cell = nb_cell_row-l_row;
      for (i;i<=nb_cell;i++)
      {
         current_cell = document.getElementById("board").rows[l_row+i*sens].cells[l_col];
         current_cell.className=type;
      }
  }
  return i;
}
function make_horizontal(type,nb_cell)
{
  var i=0;
  if (Math.abs(s_row-l_row)<=tol_line)//horizontal
  {
      var sens = Math.sign(s_col-l_col);
      if (l_col+nb_cell*sens<0)
        nb_cell = l_col+1;
      else if (l_col+nb_cell*sens>nb_cell_col)
        nb_cell = nb_cell_col-l_col;
      for (var i=0;i<=nb_cell;i++)
      {
         current_cell = document.getElementById("board").rows[l_row].cells[l_col+i*sens];
         current_cell.className=type;
      }
  }
  return i;
}

function send_data(method,path)
{
  unselect();
  const map_form = document.getElementById("data_form");
  const data_field = document.getElementById("data_field");
  var board_array = new Array();
  document.getElementById("dim_cell_cm").value = dim_cell_cm;
  var board = document.getElementById("board");
  var first_col=0;
  var last_col=0;
  var first_row=0;
  var last_row=0;
  var first_col_is_empty = true;
  var first_row_is_empty = true;
  for (var i=0; i<nb_cell_col-1;i++)
  {
    board_array.push([]);
    for (var j = 0; j < nb_cell_row-1; j++) {
      var cell = board.rows[j].cells[i];
      if (cell.className=="cell")
      {
        board_array[i].push(0);
      }
      else
      {
        if (first_col_is_empty)
        {
          first_col = i;
          first_col_is_empty = false;
        }
        last_col = i;
        if (cell.className=="wall")
          board_array[i].push(1);
        else if (cell.className=="door")
          board_array[i].push(2);
        else if (cell.className=="station")
          board_array[i].push(3);
        else
          board_array[i].push(1); //erreur
      }
    }
  }
  for (var i=0; i<nb_cell_row;i++)
  {
    for (var j = 0; j < nb_cell_col; j++) {
      var cell = board.rows[i].cells[j];
      if (cell.className!="cell")
      {
        if (first_row_is_empty)
        {
          first_row = i;
          first_row_is_empty = false;
        }
        last_row = i;
      }
    }
  };
  data_field.value="[";
  for (var i = first_col; i <= last_col; i++) {
    data_field.value+="["
    for (var j = first_row; j <= last_row; j++) {
      data_field.value+= "'"+board_array[i][j]+"',";
    }
    data_field.value = data_field.value.slice(0,-1);
    data_field.value+="],";
  }
  data_field.value = data_field.value.slice(0,-1);
  data_field.value+="]";
  //alert("Submit"+data_field.value);
  map_form.method = method;
  map_form.action = path;
  map_form.submit();
}
