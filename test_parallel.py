from multiprocessing import Process
import signal
import time
import os
import sys

class Parallel:
    def worker(self, parent):
        time.sleep(5.5)
        os.kill(parent, signal.SIGUSR1)


    def on_signal(self, signum, frame):
        self.string = "done"
        print("Time to reload")

    def main(self):
        self.string = "test"
        signal.signal(signal.SIGUSR1, self.on_signal)
        child = Process(target=self.worker, args=(os.getpid(), ))
        child.start()
        while True:
            print(f"Waiting... {self.string}")
            time.sleep(1)

p = Parallel()
p.main()