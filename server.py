import os
import signal
from aiohttp import web
import aiohttp_jinja2
import jinja2
from ast import literal_eval
import json


class OSVhttp:
    def __init__(self, parent):
        self.parent = parent
        self.app = web.Application()
        aiohttp_jinja2.setup(self.app,
                             loader=jinja2.FileSystemLoader('templates'))
        self.app.add_routes([web.get('/', self.index),
                             web.static('/static', 'templates/static'),
                             web.post('/submit_map', self.submit_map),
                             web.post('/control', self.control_robot)])

    @aiohttp_jinja2.template('index.html')
    async def index(self, request):
        return

    async def control_robot(self, request):
        data = dict(request.text())
        if data['action'] == "pause":
            os.kill(self.parent, signal.SIGSTOP)
        elif data['action'] == "continue":
            os.kill(self.parent, signal.SIGCONT)
        elif data['action'] == "reset":
            os.kill(self.parent, signal.SIGPWR)
        raise web.HTTPFound('/')
        
    async def submit_map(self, request):
        text = await request.post()
        data = dict(text)
        data["data_field"] = literal_eval(data['data_field'])
        
        with open('map.json', 'w') as f:  # Save map
            json.dump(data, f)

        os.kill(self.parent, signal.SIGHUP)  # Reload map
        raise web.HTTPFound('/')

    def schedule(self, loop):
        runner = web.AppRunner(self.app)
        loop.run_until_complete(runner.setup())
        site = web.TCPSite(runner, port=8192)
        loop.run_until_complete(site.start())